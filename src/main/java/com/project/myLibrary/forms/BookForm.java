package com.project.myLibrary.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;

@Data
public class BookForm {
    @NotEmpty
    @Length(max = 100)
    private String name;

    @NotEmpty
    @Length(max = 255)
    private String author;

    @NotEmpty
    @Length(max = 255)
    private String description;

    private MultipartFile file;
}
