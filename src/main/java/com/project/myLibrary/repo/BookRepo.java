package com.project.myLibrary.repo;

import com.project.myLibrary.models.Book;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookRepo extends CrudRepository<Book, Long> {
    List<Book> findBookByUserId(long id);

    List<Book> findAllByOrderByName();

    List<Book> findAllByUserIsNullOrderByName();

    List<Book> findAllByUserIsNotNullOrderByName();

    boolean existsBookById(long id);
}
