package com.project.myLibrary.repo;

import com.project.myLibrary.models.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepo extends CrudRepository<User, Long> {
    User findByUsername(String userName);

    User findUserById(long id);

    List<User> findAll();

    @Override
    void delete(User entity);

    @Override
    void deleteById(Long id);
}
