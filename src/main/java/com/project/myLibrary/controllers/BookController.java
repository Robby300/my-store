package com.project.myLibrary.controllers;

import com.project.myLibrary.forms.BookForm;
import com.project.myLibrary.models.Book;
import com.project.myLibrary.models.User;
import com.project.myLibrary.service.BookService;
import com.project.myLibrary.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Set;

@Controller
@RequestMapping("/library")
public class BookController {
    private final UserService userService;
    private final BookService bookService;

    public BookController(BookService bookService, UserService userService) {
        this.bookService = bookService;
        this.userService = userService;
    }

    @GetMapping()
    public String findAll(Model model) {
        Iterable<Book> books = bookService.findAllByOrderByName();
        model.addAttribute("books", books);
        return "library";
    }

    @GetMapping("/free")
    public String findAllFree(Model model) {

        Iterable<Book> books = bookService.findAllFree();
        model.addAttribute("books", books);
        return "library";
    }

    @GetMapping("/notfree")
    public String findAllNotFree(Model model) {
        Iterable<Book> books = bookService.findAllNotFree();
        model.addAttribute("books", books);
        return "library";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/add")
    public String libraryAdd() {
        return "library-add";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/library/add")
    public String libraryBookAdd(@Valid BookForm form) throws IOException {
        bookService.addBook(form);
        return "redirect:/library";
    }

    @GetMapping("/{id}")
    public String bookDetails(@PathVariable(value = "id") long id, Model model) {
        if (bookService.getString(id, model)) return "redirect:/library";
        return "library-details";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/{id}/edit")
    public String bookEdit(@PathVariable(value = "id") long id, Model model) {
        if (bookService.getString(id, model)) return "redirect:/library";
        return "library-edit";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/{id}/edit")
    public String libraryBookUpdate(@PathVariable(value = "id") long id, BookForm form) throws IOException {
        Book book = bookService.findById(id).orElseThrow(RuntimeException::new);
        book.setName(form.getName());
        book.setAuthor(form.getAuthor());
        book.setDescription(form.getDescription());
        bookService.addBookPicture(book, form.getFile());
        bookService.save(book);
        return "redirect:/library";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/{id}/remove")
    public String libraryBookDelete(@PathVariable(value = "id") long id) {
        Book book = bookService.findById(id).orElseThrow(RuntimeException::new);
        bookService.delete(book);
        return "redirect:/library";
    }

    @PostMapping("/{id}/catch")
    public String catchBook(@PathVariable(value = "id") long id) {
        catchMethod(id, true);
        return "redirect:/library/" + id;
    }

    @PostMapping("/{id}/return")
    public String returnBook(@PathVariable(value = "id") long id) {
        catchMethod(id, false);
        return "redirect:/library/" + id;
    }

    private void catchMethod(long id, boolean isBookfree) {
        Book book = bookService.findById(id).orElseThrow(RuntimeException::new);
        User user = userService.getCurrentUser();
        Set<Book> bookList = user.getBookList();
        if (isBookfree) {
            bookList.add(book);
            user.setBookList(bookList);
            book.setUser(user);
        } else {
            bookList.remove(book);
            user.setBookList(bookList);
            book.setUser(null);
        }
        bookService.save(book);
        userService.save(user);
    }
}

