package com.project.myLibrary.controllers;

import com.project.myLibrary.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    final UserService userService;

    public MainController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")        //один слэш это главная страница
    public String home(Model model) {
        model.addAttribute("title", "Главная страница");
        if (userService.getCurrentUser() != null) {
            model.addAttribute("books", userService.getBookListByCurrentUser());
        }
        return "home";
    }
}
