package com.project.myLibrary.service;

import com.project.myLibrary.forms.BookForm;
import com.project.myLibrary.models.Book;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@Service
public interface BookService {
    void addBook(BookForm bookForm) throws IOException;

    void addBookPicture(Book book, MultipartFile file) throws IOException;

    Iterable<Book> findAllByOrderByName();

    Iterable<Book> findAllFree();

    Iterable<Book> findAllNotFree();

    Optional<Book> findById(long id);

    boolean getString(@PathVariable("id") long id, Model model);

    void save(Book book);

    void delete(Book book);
}
