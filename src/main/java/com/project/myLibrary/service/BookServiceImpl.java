package com.project.myLibrary.service;

import com.project.myLibrary.forms.BookForm;
import com.project.myLibrary.models.Book;
import com.project.myLibrary.repo.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@Component
public class BookServiceImpl implements BookService {
    private final BookRepo bookRepo;

    @Value("${upload.path}")
    private String uploadPath;

    @Autowired
    public BookServiceImpl(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }


    @Override
    public void addBook(BookForm bookForm) throws IOException {
        Book book = new Book(bookForm.getName(), bookForm.getAuthor(),
                bookForm.getDescription());
        addBookPicture(book, bookForm.getFile());
        bookRepo.save(book);
    }

    @Override
    public void addBookPicture(Book book, MultipartFile file) throws IOException {
        if (file != null && !file.getOriginalFilename().isEmpty()) {
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename();
            file.transferTo(new File(uploadPath + "/" + resultFilename));
            book.setFilename(resultFilename);
        }
    }

    @Override
    public Iterable<Book> findAllByOrderByName() {
        return bookRepo.findAllByOrderByName();
    }

    @Override
    public Iterable<Book> findAllFree() {
        return bookRepo.findAllByUserIsNullOrderByName();
    }

    @Override
    public Iterable<Book> findAllNotFree() {
        return bookRepo.findAllByUserIsNotNullOrderByName();
    }

    @Override
    public Optional<Book> findById(long id) {
        return bookRepo.findById(id);
    }

    public boolean getString(@PathVariable("id") long id, Model model) {
        if (!bookRepo.existsById(id)) {
            return true;
        }
        Optional<Book> book = bookRepo.findById(id);
        ArrayList<Book> res = new ArrayList<>();
        book.ifPresent(res::add);
        model.addAttribute("books", res);
        return false;
    }

    @Override
    public void save(Book book) {
        bookRepo.save(book);
    }

    @Override
    public void delete(Book book) {
        bookRepo.delete(book);
    }
}
