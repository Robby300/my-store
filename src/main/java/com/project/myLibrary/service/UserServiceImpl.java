package com.project.myLibrary.service;

import com.project.myLibrary.models.Book;
import com.project.myLibrary.models.User;
import com.project.myLibrary.repo.BookRepo;
import com.project.myLibrary.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepo userRepo;
    private final BookRepo bookRepo;

    @Autowired
    public UserServiceImpl(UserRepo userRepo, BookRepo bookRepo) {
        this.userRepo = userRepo;
        this.bookRepo = bookRepo;
    }

    public List<User> findAll() {
        return userRepo.findAll();
    }

    public void save(User user) {
        userRepo.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepo.findByUsername(username);
    }

    public String getCurrentUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName();
    }

    public User getCurrentUser() {
        return (User) loadUserByUsername(getCurrentUsername());
    }

    public List<Book> getBookListByUserId(Long id) {
        return bookRepo.findBookByUserId(id);
    }

    public List<Book> getBookListByCurrentUser() {
        return bookRepo.findBookByUserId(getCurrentUser().getId());
    }

    public void deleteById(Long id) {
        userRepo.deleteById(id);
    }

    public User findUserById(long id) {
        return userRepo.findUserById(id);
    }

    public void booksAway(long id) {
        User user = userRepo.findUserById(id);
        Set<Book> bookList = user.getBookList();
        for (Book book : bookList) {
            book.setUser(null);
        }
        user.setBookList(null);
        userRepo.save(user);
    }

    public User findByUsername(String username) {
        return userRepo.findByUsername(username);
    }
}
