package com.project.myLibrary.service;

import com.project.myLibrary.models.Book;
import com.project.myLibrary.models.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    public List<User> findAll();

    public void save(User user);

    @Override
    public UserDetails loadUserByUsername(String username);

    public String getCurrentUsername();

    public User getCurrentUser();

    public List<Book> getBookListByUserId(Long id);

    public List<Book> getBookListByCurrentUser();

    public void deleteById(Long id);

    public User findUserById(long id);

    public void booksAway(long id);

    public User findByUsername(String username);

}
