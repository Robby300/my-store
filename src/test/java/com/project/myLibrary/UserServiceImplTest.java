package com.project.myLibrary;

import com.project.myLibrary.models.Role;
import com.project.myLibrary.models.User;
import com.project.myLibrary.repo.UserRepo;
import com.project.myLibrary.service.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {
    @Mock
    UserRepo userRepo;

    @InjectMocks
    UserServiceImpl userService;

    public User returnUser = User.builder()
            .username("Oleg").active(true)
            .id(1L).roles(Collections.singleton(Role.USER))
            .password("123").build();

    @Test
    void returnedUserByIdIsCorrect() {
        when(userRepo.findUserById(1L)).thenReturn(returnUser);

        User user = userService.findUserById(1);
        assertFalse(user == null);
        assertThat(user.getId(), equalTo(1L));
        assertThat(user.getUsername(), equalTo("Oleg"));
        assertTrue(user.getRoles().contains(Role.USER));
        assertThat(user.getPassword(), equalTo("123"));
    }
}
