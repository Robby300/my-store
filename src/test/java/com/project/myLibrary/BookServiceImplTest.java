package com.project.myLibrary;

import com.project.myLibrary.models.Book;
import com.project.myLibrary.repo.BookRepo;

import com.project.myLibrary.service.BookServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BookServiceImplTest {

    @Mock
    BookRepo bookRepo;

    @InjectMocks
    BookServiceImpl bookService;

    // Mock book

    /*public Book returnFirstBook = new Book(1L, "Robinson Cruzo", "Daniel Defo",
            "Robinson just wanted to buy some slaves", null, "image.jpg");*/

    public Book returnSecondBook = Book.builder().id(2L).author("Jack London")
            .name("White Fang").description("The book about a wild wolfdog").filename("secondImage.jpg")
            .build();

    @Test
    void returnedBookByIdIsCorrect() {

        when(bookRepo.findById(2L)).thenReturn(Optional.ofNullable(returnSecondBook));

        Optional<Book> book2 = bookService.findById(2L);

        assertFalse(book2.isEmpty());
        assertThat(book2.get().getId(), equalTo(2L));
        assertThat(book2.get().getName(), equalTo("White Fang"));
        assertThat(book2.get().getDescription(), equalTo("The book about a wild wolfdog"));
        assertThat(book2.get().getAuthor(), equalTo("Jack London"));
        assertThat(book2.get().getFilename(), equalTo("secondImage.jpg"));
    }
}
